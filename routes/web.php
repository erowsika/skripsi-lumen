<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*
|--------------------------------------------------------------------------
| Authentication Route
|--------------------------------------------------------------------------
*/
$router->post('/login', 'UserController@authenticate');

/*
|--------------------------------------------------------------------------
| User Route
|--------------------------------------------------------------------------
*/
$router->get('/user', 'UserController@index');
$router->post('/user', 'UserController@store');
$router->get('/user/{id}', 'UserController@show');
$router->put('/user/{id}', 'UserController@update');
$router->delete('/user/{id}', 'UserController@destroy');

/*
|--------------------------------------------------------------------------
| Pegawai Route
|--------------------------------------------------------------------------
*/
$router->get('/pegawai', 'PegawaiController@index');
$router->post('/pegawai', 'PegawaiController@store');
$router->get('/pegawai/{id}', 'PegawaiController@show');
$router->put('/pegawai/{id}', 'PegawaiController@update');
$router->delete('/pegawai/{id}', 'PegawaiController@destroy');

/*
|--------------------------------------------------------------------------
| Kriteria Route
|--------------------------------------------------------------------------
*/
$router->get('/kriteria', 'KriteriaController@index');
$router->post('/kriteria', 'KriteriaController@store');
$router->get('/kriteria/{id}', 'KriteriaController@show');
$router->put('/kriteria/{id}', 'KriteriaController@update');
$router->delete('/kriteria/{id}', 'KriteriaController@destroy');

/*
|--------------------------------------------------------------------------
| Hasil Penilaian Route
|--------------------------------------------------------------------------
*/
$router->post('/hasil_penilaian', 'HasilPenilaianController@store');

/*
|--------------------------------------------------------------------------
| Waktu Penilaian Promethee Route
|--------------------------------------------------------------------------
*/
$router->get('/waktu_penilaian_promethee', 'WaktuPenilaianPrometheeController@index');
$router->get('/waktu_penilaian_promethee/{id}', 'WaktuPenilaianPrometheeController@show');
$router->get('/is_waktu_penilaian_promethee/{id}', 'WaktuPenilaianPrometheeController@is_promethee');

/*
|--------------------------------------------------------------------------
| Waktu Penilaian Topsis Route
|--------------------------------------------------------------------------
*/
$router->get('/waktu_penilaian_topsis', 'WaktuPenilaianTopsisController@index');
$router->get('/waktu_penilaian_topsis/{id}', 'WaktuPenilaianTopsisController@show');
