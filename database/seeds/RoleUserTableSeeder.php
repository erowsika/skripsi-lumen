<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // pejabat pimpinan dengan role admin
        DB::table('role_user')->insert([
            'id_user' => 1,
            'id_role' => 1
        ]);
        
        // pejabat sekretaris dengan role admin
        DB::table('role_user')->insert([
            'id_user' => 2,
            'id_role' => 1
        ]);
        
        // pejabat karyawan dengan role user
        DB::table('role_user')->insert([
            'id_user' => 3,
            'id_role' => 2
        ]);
        
        // admin dengan role admin
        DB::table('role_user')->insert([
            'id_user' => 4,
            'id_role' => 1
        ]);
    }
}
