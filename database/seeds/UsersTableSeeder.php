<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->username = '01';
        $user->password = Crypt::encrypt('admin');
        $user->api_token = null;
        $user->save();

        $user = new \App\User;
        $user->username = '02';
        $user->password = Crypt::encrypt('admin');
        $user->api_token = null;        
        $user->save();

        $user = new \App\User;
        $user->username = 'MNig';
        $user->password =  Crypt::encrypt('user');
        $user->api_token = null;        
        $user->save();

        $user = new \App\User;
        $user->username = 'admin';
        $user->password = Crypt::encrypt('admin');
        $user->api_token = null;        
        $user->save();
    }
}
