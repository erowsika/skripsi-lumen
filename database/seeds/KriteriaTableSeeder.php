<?php

use App\Models\Kriteria;
use Illuminate\Database\Seeder;

class KriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kriteria = Kriteria::insert([
            [
                'name' => 'Kesetiaan',
                'bobot' => '5'
            ],
            [
                'name' => 'Prestasi kerja',
                'bobot' => '3'
            ],
            [
                'name' => 'Tanggung jawab',
                'bobot' => '2'
            ],
            [
                'name' => 'Ketaatan',
                'bobot' => '2'
            ],
            [
                'name' => 'Kejujuran',
                'bobot' => '4'
            ],
            [
                'name' => 'Kerja sama',
                'bobot' => '3'
            ],
            [
                'name' => 'Prakarsa',
                'bobot' => '2'
            ],
            [
                'name' => 'Kepemimpinan',
                'bobot' => '2'
            ]
        ]);
    }
}
