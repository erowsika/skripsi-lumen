<?php

use App\Models\Pegawai;
use Illuminate\Database\Seeder;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now();

        $q = Pegawai::insert([
            // [
            //     "name" => "Andy Mauritz MK, S.Sos., M.M.",
            //     "nip" => "197412231998031003",
            //     "pangkat" => "Pembina",
            //     "golongan" => "IV a",
            //     "jabatan" => "Camat",
            //     "created_at" => $now,
            //     "updated_at" => $now
            // ],
            // [
            //     "name" => "Al Azhar Achmad, S.STP.",
            //     "nip" => "197903141998021001",
            //     "pangkat" => "Penata Tingkat I",
            //     "golongan" => "III d",
            //     "jabatan" => "Sekcam",
            //     "created_at" => $now,
            //     "updated_at" => $now
            // ],
            [
                "name" => "Mahfuddin Gassing, S.E.",
                "nip" => "196408311985031006",
                "pangkat" => "Penata Tingkat I",
                "golongan" => "III d",
                "jabatan" => "Kasi Pembina Kelurahan/Desa",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "M. Arsyad Umar, S.Pd.",
                "nip" => "196103071986111001",
                "pangkat" => "Penata Tingkat I",
                "golongan" => "III d",
                "jabatan" => "Kasi Pemerintahan",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Bahtiar Usman, S.Sos.",
                "nip" => "196312971985931011",
                "pangkat" => "Penata Tingkat I",
                "golongan" => "III d",
                "jabatan" => "Kasi Pemberdayaan Masyarakat",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Syakhrir, S.Sos.",
                "nip" => "196607161991021003",
                "pangkat" => "Penata Tingkat I",
                "golongan" => "III b",
                "jabatan" => "Kasi Ketentraman",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Asmi, S.Sos.",
                "nip" => "197505282010012011",
                "pangkat" => "Penata Muda Tingkat I",
                "golongan" => "III c",
                "jabatan" => "Kasubag Umum & Kepegawaian",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Andi Harni Riska W, S.T.",
                "nip" => "198404222006042011",
                "pangkat" => "Penata Muda Tingkat I",
                "golongan" => "III b",
                "jabatan" => "Kasubag Perencanaan & Keuangan",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Syamsul",
                "nip" => "196208261986111001",
                "pangkat" => "Pengatur Muda Tingkat I",
                "golongan" => "II b",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Muhammad Saleh",
                "nip" => "196710032014091001",
                "pangkat" => "Pengatur Muda",
                "golongan" => "II a",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Ruslan",
                "nip" => "196702172009011002",
                "pangkat" => "Pengatur Muda Tingkat I",
                "golongan" => "II b",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Muhammad Yacub",
                "nip" => "196012311988021013",
                "pangkat" => "Pengatur Tingkat I",
                "golongan" => "II d",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Ramli",
                "nip" => "197709292014071001",
                "pangkat" => "Pengatur Muda",
                "golongan" => "II a",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Nurdiana Wahab",
                "nip" => "198409052009012004",
                "pangkat" => "Pengatur",
                "golongan" => "II c",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Junaid",
                "nip" => "196309042014071001",
                "pangkat" => "Pengatur Muda",
                "golongan" => "II a",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Hadiah",
                "nip" => "197008282014102001",
                "pangkat" => "Pengatur Muda",
                "golongan" => "II a",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ],
            [
                "name" => "Murni",
                "nip" => "196801272014072001",
                "pangkat" => "Pengatur Muda",
                "golongan" => "II a",
                "jabatan" => "Staff",
                "created_at" => $now,
                "updated_at" => $now
            ]
            // ,[
            //     "name" => "",
            //     "nip" => "",
            //     "pangkat" => "",
            //     "golongan" => "",
            //     "jabatan" => "",
            //     "created_at" => $now,
            //     "updated_at" => $now
            // ]
        ]);
    }
}
