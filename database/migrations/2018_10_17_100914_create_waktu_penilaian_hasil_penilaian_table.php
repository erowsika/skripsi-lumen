<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaktuPenilaianHasilPenilaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waktu_penilaian_hasil_penilaian', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_waktu_penilaian');
            $table->integer('id_hasil_penilaian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waktu_penilaian_hasil_penilaian');
    }
}
