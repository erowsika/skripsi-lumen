<?php

namespace App\Repository;

use App\User;
use App\Models\Kriteria;
use App\Models\WaktuPenilaian;
use Illuminate\Support\Facades\DB;

class WaktuPenilaianTopsisRepository {

    protected $model;

    public function __construct(WaktuPenilaian $waktuPenilaian) {
        $this->model = $waktuPenilaian;
    }

    public function show($waktuPenilaianId) {
        $query = $this->model->findOrFail($waktuPenilaianId);

        return [
            'tanggal' => $query->tanggal,
            'waktu_penilaian_topsis' => $query->hasilPenilaian
        ];
    }

    public function hasil($waktuPenilaianId) {
         // xij
        $query = $this->model->findOrFail($waktuPenilaianId);

        $Mkriteria = Kriteria::all();
        $karyawan = User::all();

        // $query2 = DB::table('hasil_penilaian')
        // ->join('pegawai', 'pegawai.id', '=', 'hasil_penilaian.id_karyawan')
        // ->join('kriteria', 'kriteria.id', '=', 'hasil_penilaian.id_kriteria')
        // ->selectRaw(
        //     'pegawai.name as nama_alternatif, kriteria.name as nama_kriteria, hasil_penilaian.nilai, kriteria.bobot'
        // )
        // ->get();

        $query2 = $query->hasilPenilaian()        
        ->join('pegawai', 'pegawai.id', '=', 'hasil_penilaian.id_karyawan')
        ->join('kriteria', 'kriteria.id', '=', 'hasil_penilaian.id_kriteria')
        ->selectRaw(
            'pegawai.name as nama_alternatif, kriteria.name as nama_kriteria, hasil_penilaian.nilai, kriteria.bobot'
        )
        ->get();

        $data           = array();
        $kriterias      = array();
        $bobot          = array();
        $nilai_kuadrat  = array();

        $xij = $rij =  $yij = array();
        
        // data
        foreach ($query2 as $index=>$row) {
            if(!isset($data[$row->nama_alternatif])){
                $data[$row->nama_alternatif]=array();
            }
            if(!isset($data[$row->nama_alternatif][$row->nama_kriteria])){
            $data[$row->nama_alternatif][$row->nama_kriteria]=array();
            }
            if(!isset($nilai_kuadrat[$row->nama_kriteria])) $nilai_kuadrat[$row->nama_kriteria] = 0;

            $bobot[$row->nama_kriteria] = $row->bobot;
            $data[$row->nama_alternatif][$row->nama_kriteria] = $row->nilai;
            $nilai_kuadrat[$row->nama_kriteria] += pow($row->nilai,2);
            $kriterias[] = $row->nama_kriteria;            
        }

        $kriteria = array_unique($kriterias);

        // rij
        // $rij = $data;

        foreach ($data as $nama => $krit) {
            foreach ($kriteria as $k) {
                $temp = $krit[$k] / sqrt($nilai_kuadrat[$k]);
                $rijr[$nama][$k] = round( $temp, 4);
                $rij[$nama][$k] = $temp;
            }
        }

        // yij
        // $yij = $data;
        
        $i=0;
        $y=array();
        foreach ($data as $nama => $krit) {
            ++$i;
            foreach ($kriteria as $k) {
                $temp = $bobot[$k] * $rij[$nama][$k];
                $y[$k][$i-1] = $temp;
                $yijr[$nama][$k] = round($temp, 4);
                $yij[$nama][$k] = $temp;
            }
        }

        // A+
        $aplus = array();
        foreach ($kriteria as $k) {
            $temp = ( [$k] ? max($y[$k]) : min($y[$k]) );
            $aplusr[$k] = round($temp, 4);
            $aplus[$k] = $temp;
        }

        // A-
        $aminus = array();
        foreach ($kriteria as $k) {
            $temp = ( [$k] ? min($y[$k]) : max($y[$k]) );
            $aminusr[$k] = round($temp, 4);
            $aminus[$k] = $temp;
        }

        // Di+
        $diplus = array();
        $i=0;
        foreach ($data as $nama => $krit) {
            ++$i;
            foreach ($kriteria as $k) {
                if (!isset($diplusr[$i-1]) && !isset($diplus[$i-1])) $diplus[$i-1] = $diplusr[$i-1] = 0;
                $diplus[$i-1] += pow($aplus[$k] - $y[$k][$i-1], 2);
            }
            $temp = sqrt($diplus[$i-1]);
            $diplusr[$i-1] = round($temp, 4);
            $diplus[$i-1] = $temp;
            
        }

        // Di-
        $diminus = array();
        $i=0;
        foreach ($data as $nama => $krit) {
            ++$i;
            foreach ($kriteria as $k) {
                if ( !isset($diminus[$i-1]) ) $diminus[$i-1] = 0;
                $diminus[$i-1] += pow($aminus[$k] - $y[$k][$i-1], 2);
            }
            $temp = sqrt($diminus[$i-1]);
            $diminusr[$i-1] = round($temp, 4);
            $diminus[$i-1] = $temp;
        }

        // RCi+
        $rciplus = array();
        $i=0;   
        foreach ($data as $nama => $krit) {
            ++$i;
            foreach ($kriteria as $k) {
                $temp =  $diminus[$i-1] / ( $diminus[$i-1] + $diplus[$i-1] );
                $rciplusr[$i-1] = round($temp, 4);
                $rciplus[$i-1] = $temp;
            }
        }

        return [
            'tanggal' => $query->tanggal,
            'waktu_penilaian_topsis' => [
                'kriterias' => $kriterias,
                'xij' => $query->hasilPenilaian,
                'rij' => $rijr,
                'yij' => $yijr,
                'query2' => $query2,
                'aplus' => $aplusr,
                'aminus' => $aminusr,
                'diplus' => $diplusr,
                'diminus' => $diminusr,
                'rciplus' => $rciplusr,
                'y' => $y,
                'data' => $data,
                'nilai_kuadrat' => $nilai_kuadrat,
                'bobot' => $bobot
            ]
        ];
    }

    // SELECT b.name,c.name,a.nilai,c.name
    //   FROM
    //     topsis a
    //     JOIN
    //       users b ON b.id = a.id_karyawan
    //     JOIN
    //       kriteria c ON c.id = a.id_kriteria
}