<?php

namespace App\Repository;

use App\User;
use App\Models\Kriteria;
use App\Models\WaktuPenilaian;
use Illuminate\Support\Facades\DB;

class WaktuPenilaianPrometheeRepository {

    protected $model;

    public function __construct(WaktuPenilaian $waktuPenilaian) {
        $this->model = $waktuPenilaian;
    }

    public function show($id) {
        $query = $this->model->findOrFail($id);

        return [
            'tanggal' => $query->tanggal,
            'waktu_penilaian_promethee' => $query->promethee
        ];
    }

    public function hasil($id) {

        $query = $this->model->findOrFail($id);
        $Mkriteria = Kriteria::all()->pluck('bobot');

        $alt = '';

        foreach ($query->hasilPenilaian as $key => $value) {
            $temp = $value->subKriteria->where('id_kriteria', $value->id_kriteria)->first();
            $sub_kriteria[$value->id_kriteria] = $temp;


            if ($alt != $value->id_karyawan) {
                $alt = $value->id_karyawan;
                $alts[$value->id_karyawan] = $value->users->name;
                $data[$value->id_karyawan] = array();
            }

            $data[$value->id_karyawan][$temp->id] = $value->nilai;
            // $bobot[$value->id_kriteria] = $value->bobot;
        }

        // Deviasi 
        foreach ($alts as $code_A => $name_A) {
            $d[$code_A] = array();
            
            foreach ($alts as $code_B => $name_B) {
                if ($code_A != $code_B) {
                    $d[$code_A][$code_B] = array();
                    
                    foreach ($sub_kriteria as $sub => $v) {
                        $d[$code_A][$code_B][$sub] = $data[$code_A][$v->id] - $data[$code_B][$v->id];
                    }
                }
            }
        }
        
        // fungsi preferensi
        foreach ($alts as $code_A => $name_A) {
            $P[$code_A] = array();
            
            foreach ($alts as $code_B => $name_B) {
                if ($code_A != $code_B) { 
                    $P[$code_A][$code_B] = array(); 

                    foreach ($sub_kriteria as $sub => $v) {
                        $P[$code_A][$code_B][$sub] = $this->preference($d[$code_A][$code_B][$sub], $sub_kriteria[$sub]);
                        // * $Mkriteria[$sub-1]; 
                    } 
                } 
            }
        }

        // Indeks Preferensi Global
        $j = count($sub_kriteria); 
        $sigma = array(); 
        foreach ($alts as $code_A => $name_A) { 
            $sigma[$code_A] = array(); 

            foreach ($alts as $code_B => $name_B) { 
                if ($code_A != $code_B) { 
                    // $sigma[$code_A][$code_B] = array_sum($P[$code_A][$code_B]) / $j; 
                    $sigma[$code_A][$code_B] = 1 / $j * (array_sum($P[$code_A][$code_B])); 
                } 
            } 
        } 

        //-- menghitung Leaving Flow 
        $leaving_flow = array(); 
        $devider = count($sigma) - 1; 
        foreach ($sigma as $code_A => $value_A) { 
            // $leaving_flow[$code_A] = array_sum($value_A) / $devider; 
            $leaving_flow[$code_A] = (1 / $devider) * array_sum($value_A);
        }

        //-- menghitung Entering Flow 
        $entering_flow = array(); 
        foreach ($sigma as $code_A => $item_A) { 
            foreach ($item_A as $code_B => $value_B) { 
                if (!isset($entering_flow[$code_B])) $entering_flow[$code_B] = 0; 
                $entering_flow[$code_B] += $value_B;
            } 
        }

        foreach ($sigma as $code_A => $value_A) { 
            $entering_flow[$code_A] /= $devider;
            // $entering_flow[$code_A] = (1 / $devider) * $entering_flow[$code_A];
        }

        //-- Menghitung Net-Flow 
        $net_flow = array(); 
        foreach ($leaving_flow as $code_A => $value_A) { 
            $net_flow[$code_A] = $value_A - $entering_flow[$code_A]; 
        }

        //-- Menentukan peringkat lengkapnya 
        arsort($net_flow);

        // round lf ef nf
        foreach ($leaving_flow as $key => $value) {
            $leaving_flow[$key] = round($leaving_flow[$key], 4);
            $entering_flow[$key] = round($entering_flow[$key], 4);
            $net_flow[$key] = round($net_flow[$key], 4);
        }

        return [
            'tanggal' => $query->tanggal,
            'waktu_penilaian_promethee' => [
                'xij' => $query->hasilPenilaian,
                'alts' => $alts,
                // 'q2' => $query2,
                'sub_kriteria' => $sub_kriteria,
                'data' => $data,
                'deviasi' => $d,
                'preferensi' => $P,
                'ipg' => $sigma,
                'lf' => $leaving_flow,
                'ef' => $entering_flow,
                'nf' => $net_flow
            ]
        ];
    }

    public function is_promethee($id) {

        return $this->model
        ->findOrFail($id)
        ->hasilPenilaian()->whereHas('subKriteria', function ($query) { $query->where('s', '=', 0.00)->where('p', '=', 0.00)->where('q', '=', 0.00); })->count() > 0 ? true : false;

        // $q->hasilPenilaian()->whereHas('subKriteria', function ($query) { $query->where('s', '=', 0.00); })->get()
        // $q->hasilPenilaian()->whereHas('subKriteria', function ($query) { $query->where('s', '!=', 0.00)->where('p', '!=', 0.00)->where('q', '!=', 0.00); })->get()
    }

    function preference($value, $sub) { 
        //-- usual
        if ($sub['tipe_preferensi'] == "1") return $value == 0 ? 0 : 1;

         //-- linear
        elseif($sub['tipe_preferensi'] == "2") return $value == 0 ? 0 : ($value > $sub['p'] ? 1 : $value / $sub['p']);
        
        //-- quasi
        elseif($sub['tipe_preferensi'] == "3") return $value <= $sub['q'] ? 0 : 1;

        //-- linear quasi
        elseif($sub['tipe_preferensi'] == "4") return $value < $sub['q'] ? 0 : ($value > $sub['p'] ? 1 : $value / ($sub['p'] - $sub['q']));

        //-- level
        elseif($sub['tipe_preferensi'] == "5") return $value == $sub['q'] ? 0 : ($value > $sub['p'] ? 1 : 1/2); 

        //-- gaussian 
        elseif($sub['tipe_preferensi'] == "6") return $value == 0 ? 0 : 1 - exp(-1 * pow($value,2) / (2 * pow($sub['s'] ,2)));
    } 
}