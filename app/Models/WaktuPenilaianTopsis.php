<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaktuPenilaianTopsis extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'waktu_penilaian_topsis';

    public function topsis()
    {
        return $this->belongsToMany('App\Models\topsis', 'waktu_penilaian_topsis_topsis', 'id_waktu_penilaian_topsis', 'id_topsis');
    }
}
