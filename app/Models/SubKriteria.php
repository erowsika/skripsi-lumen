<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubKriteria extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_kriteria';

    public function hasilPenilaian()
    {
        return $this->belongsToMany('App\Models\HasilPenilaian', 'sub_kriteria_hasil_penilaian', 'id_sub_kriteria', 'id_hasil_penilaian');
    }

    public function getIsPrometheeAttribute()
    {
        if ($this->attributes['q'] == 0.00 && $this->attributes['p'] == 0.00 && $this->attributes['s'] == 0.00 ) return true;
        return false; 
    }
}
