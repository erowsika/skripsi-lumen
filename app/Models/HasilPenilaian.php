<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HasilPenilaian extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hasil_penilaian';

    public function waktuPenilaian()
    {
        return $this->belongsToMany('App\Models\WaktuPenilaian', 'waktu_penilaian_hasil_penilaian', 'id_hasil_penilaian', 'id_waktu_penilaian');
    }

    public function subKriteria()
    {
        return $this->belongsToMany('App\Models\SubKriteria', 'sub_kriteria_hasil_penilaian', 'id_hasil_penilaian', 'id_sub_kriteria');
    }

    public function users(){
        return $this->belongsTo('App\Models\Pegawai', 'id_karyawan');
    }
}
