<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaktuPenilaian extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'waktu_penilaian';

    public function hasilPenilaian()
    {
        return $this->belongsToMany('App\Models\HasilPenilaian', 'waktu_penilaian_hasil_penilaian', 'id_waktu_penilaian', 'id_hasil_penilaian');
    }
}
