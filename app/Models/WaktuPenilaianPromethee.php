<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaktuPenilaianPromethee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'waktu_penilaian_promethee';

    public function promethee()
    {
        return $this->belongsToMany('App\Models\promethee', 'waktu_penilaian_promethee_promethee', 'id_waktu_penilaian_promethee', 'id_promethee');
    }
}
