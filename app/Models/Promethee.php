<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promethee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promethee';

    public function waktuPenilaianPromethee()
    {
        return $this->belongsToMany('App\Models\WaktuPenilaianPromethee', 'waktu_penilaian_promethee_promethee', 'id_promethee', 'id_waktu_penilaian_promethee');
    }
}
