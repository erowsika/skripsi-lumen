<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topsis extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'topsis';

    public function waktuPenilaianTopsis()
    {
        return $this->belongsToMany('App\Models\WaktuPenilaianTopsis', 'waktu_penilaian_topsis_topsis', 'id_topsis', 'id_waktu_penilaian_topsis');
    }
}
