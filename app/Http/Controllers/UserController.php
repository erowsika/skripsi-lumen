<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
            'except' => 'authenticate'
        ]);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('username', $request->input('username'))->first();

        $decrypted = Crypt::decrypt($user->password);
        try {
        } catch (DecryptException $e) {
            //
        }

        if ($request->input('password') === $decrypted) {
            $user->generateToken();

            return response()->json([
                'id' => $user->id,
                'username' => $user->username,
                'token' => $user->makeVisible('api_token')->api_token
            ], 200);
        }

        return response()->json('Unauthorised', 401);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::with('roles:role')->orderBy('id','asc');

        if ($request->query('page') && $request->query('order')) {
            switch ($request->query('order')) {
                case 'desc':
                    $users->getQuery()->orders = null;
                    $users->orderBy('id','desc');
                    break;
            }
        }

        if ($request->query('sort')) {
            $sort = explode('|', $request->query('sort'));
            if ($sort[0] == 'roles') {
                $users->getQuery()->orders = null;
                // $users->join('roles', 'users.id', '=', 'roles.id')
                // ->orderBy('roles.roles', $sort[1]);
                $users->orderBy('roles', $sort[1]);
            } else {
                $users->getQuery()->orders = null;
                $users->orderBy($sort[0], $sort[1]);
            }
        }

        if ($request->query('filter')) {
            $users->getQuery()->where = null;            
            $users->where('username', 'like', "%{$request->query('filter')}%");
        }

        return response()->json(
            $users->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'password' => 'required|min:2'
        ]);

        $user = new User();
        $user->username = $request->username;
        $user->password = Crypt::encrypt($request->password);

        if($user->save()) {
            $user->roles()->attach($request->role);
            return response()->json('success');
        }
        return response()->json('error', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username,'.$id,
            'password' => 'required|min:2'
        ]);

        $user = User::find($id);
        $user->username = $request->get('username');
        $user->password = Crypt::encrypt($request->get('password'));

        
        if ($user->save()) {
            if (count($request->get('role')) > 1)
            {
                // Transform data
                $transformed_data = collect($request->get('role'))->map(function ($item) {
                    return (int) $item;
                });

                $user->roles()->sync($transformed_data);
            } 
            else if ($request->get('username') == 'admin') {
                $user->roles()->sync([ (int) $request->role ]);
            }else {
                $user->roles()->sync([ (int) $request->role ]);
            }

            return response()->json('success');
        }
        else return response()->json('error', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::find($id);
        $username = $users->username;
        if ($users->delete()) return response()->json('success');
        return response()->json('error', 400);
    }
}
