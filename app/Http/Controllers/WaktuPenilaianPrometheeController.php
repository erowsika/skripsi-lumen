<?php

namespace App\Http\Controllers;

use App\Repository\WaktuPenilaianPrometheeRepository;
use App\Http\Controllers\Controller;
use App\Models\WaktuPenilaian;
use Illuminate\Http\Request;

class WaktuPenilaianPrometheeController extends Controller
{
    protected $waktuPenilaianPrometheeRepository;

    public function __construct(WaktuPenilaianPrometheeRepository $waktuPenilaianPrometheeRepository) {
        $this->middleware('auth');
        $this->waktuPenilaianPrometheeRepository = $waktuPenilaianPrometheeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $waktuPenilaian = WaktuPenilaian::orderBy('id','desc');
        
        if ($request->query('page') && $request->query('order')) {
            switch ($request->query('order')) {
                case 'asc':
                    $waktuPenilaian->getQuery()->orders = null;
                    $waktuPenilaian->orderBy('id','asc');
                    break;
            }
        }

        if ($request->query('sort')) {
            $sort = explode('|', $request->query('sort'));
            $waktuPenilaian->getQuery()->orders = null;
            $waktuPenilaian->orderBy($sort[0], $sort[1]);
        }

        if ($request->query('filter')) {
            $waktuPenilaian->getQuery()->where = null;            
            $waktuPenilaian->where('tanggal', 'like', "%{$request->query('filter')}%");
        }
        
        return response()->json(
            $waktuPenilaian->paginate()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!$this->waktuPenilaianPrometheeRepository->is_promethee($id)) return response()->json($this->waktuPenilaianPrometheeRepository->hasil($id));
        else return response()->json('false', 404);
    }

    public function is_promethee($id)
    {
        if (!$this->waktuPenilaianPrometheeRepository->is_promethee($id)) return response()->json();
        return response()->json('false', 404);
    }
}