<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\WaktuPenilaian;
use App\Models\HasilPenilaian;
use Illuminate\Http\Request;
use App\Models\SubKriteria;

class HasilPenilaianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_array($request->input('pegawai'))) {
            $WaktuPenilaian = new WaktuPenilaian();
            $WaktuPenilaian->tanggal = $request->input('waktu_penilaian');
            $WaktuPenilaian->save();

            $idSubKriteria = [];

            foreach ($request->input('sub_kriteria') as $key => $value) {
                $subKriteria = new SubKriteria();
                $subKriteria->id_kriteria = $key;
                $subKriteria->min_max = $value['minMax'];
                $subKriteria->tipe_preferensi = $value['tipePreferensi'];
                $subKriteria->p = doubleval($value['p']);
                $subKriteria->q = doubleval($value['q']);
                $subKriteria->s = $value['s'];
                $subKriteria->save();
                array_push($idSubKriteria, $subKriteria->id);
            }

            foreach ($request->input('pegawai') as $key => $value) {
                $hasilPenilaian = new HasilPenilaian();
                $hasilPenilaian->id_pegawai = $value['id_pegawai'];
                $hasilPenilaian->id_kriteria = $value['id_kriteria'];
                $hasilPenilaian->nilai = $value['nilai'];
                $hasilPenilaian->save();
                $hasilPenilaian->waktuPenilaian()->attach($WaktuPenilaian->id);
                $hasilPenilaian->subKriteria()->syncWithoutDetaching($idSubKriteria);
            }
            return response()->json('success');
        }
    }
}