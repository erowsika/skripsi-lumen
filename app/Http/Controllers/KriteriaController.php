<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kriteria;

class KriteriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->query('page')) {
            $kriteria = Kriteria::all();

            return response()->json(
                $kriteria
            );
        }

        $kriteria = Kriteria::orderBy('id','asc');

        if ($request->query('page') && $request->query('order')) {
            switch ($request->query('order')) {
                case 'desc':
                    $kriteria->getQuery()->orders = null;
                    $kriteria->orderBy('id','desc');
                    break;
            }
        }

        if ($request->query('sort')) {
            $sort = explode('|', $request->query('sort'));
            $kriteria->getQuery()->orders = null;
            $kriteria->orderBy($sort[0], $sort[1]);
        }

        if ($request->query('filter')) {
            $kriteria->getQuery()->where = null;            
            $kriteria->where('name', 'like', "%{$request->query('filter')}%");
        }

        return response()->json(
            $kriteria->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:kriteria,name',
            'bobot' => 'required'
        ]);

        $kriteria = new Kriteria();
        $kriteria->name = $request->name;
        $kriteria->bobot = $request->bobot;

        if ($kriteria->save()) return response()->json('success');
        return response()->json('success', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kriteria = kriteria::findOrFail($id);

        return response()->json($kriteria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:kriteria,name,'.$id,
            'bobot' => 'required'
        ]);

        $kriteria = Kriteria::findOrFail($id);
        $kriteria->name = $request->name;
        $kriteria->bobot = $request->bobot;

        if ($kriteria->save()) return response()->json('success');
        return response()->json('success', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kriteria = Kriteria::findOrFail($id);

        if ($kriteria->delete()) return response()->json('success');
        return response()->json('success', 400);
    }
}