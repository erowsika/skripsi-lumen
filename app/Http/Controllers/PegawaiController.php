<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pegawai;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pegawai = Pegawai::orderBy('id','asc');
        
        if ($request->query('page') && $request->query('order')) {
            switch ($request->query('order')) {
                case 'desc':
                    $pegawai->getQuery()->orders = null;
                    $pegawai->orderBy('id','desc');
                    break;
            }
        }

        if ($request->query('sort')) {
            $sort = explode('|', $request->query('sort'));
            $pegawai->getQuery()->orders = null;
            $pegawai->orderBy($sort[0], $sort[1]);
        }

        if ($request->query('filter')) {
            $pegawai->getQuery()->where = null;            
            $pegawai->where('name', 'like', "%{$request->query('filter')}%");
        }
        
        return response()->json(
            $pegawai->paginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'nip' => 'required|unique:pegawai,nip',
            'golongan' => 'required',
            'pangkat' => 'required',
            'jabatan' => 'required'
        ]);

        $pegawai = new Pegawai();
        $pegawai->name = $request->name;
        $pegawai->nip = $request->nip;
        $pegawai->golongan = $request->golongan;
        $pegawai->pangkat = $request->pangkat;
        $pegawai->jabatan = $request->jabatan;

        if($pegawai->save()) return response()->json('success');
        return response()->json('success', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pegawai = Pegawai::findOrFail($id);

        return response()->json($pegawai);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'nip' => 'required|unique:pegawai,nip,'.$id,
            'golongan' => 'required',
            'pangkat' => 'required',
            'jabatan' => 'required'
        ]);

        $pegawai = Pegawai::findOrFail($id);
        $pegawai->name = $request->name;
        $pegawai->nip = $request->nip;
        $pegawai->golongan = $request->golongan;
        $pegawai->pangkat = $request->pangkat;
        $pegawai->jabatan = $request->jabatan;

        if ($pegawai->save()) return response()->json('success');
        return response()->json('success', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        if ($pegawai->delete()) return response()->json('success');
        return response()->json('success', 400);
    }
}